import unittest
import xmlrunner

import sys
import os
# getting the name of the directory
# where the this file is present.
current = os.path.dirname(os.path.realpath(__file__))
root = os.path.join(current, f'..{os.path.sep}..')
sys.path.append(root)

from mylib.src.lib import merge


XML_RESULT_FILE = os.path.join(root, f'report.xml')


cases = [
  {
    'test': [[('hello', 1), ('hello1', 1)]],
    'answer': {'hello': 1, 'hello1': 1},
  },
  {
    'test': [[('hello', 1), ('hello', 1)]],
    'answer': {'hello': 2},
  },
]

class TestWordCount(unittest.TestCase):
  # setUp method is overridden from the parent class TestCase
  def setUp(self):
    pass

  def tearDown(self):
    pass

  # Each test method starts with the keyword test_
  def test_merge(self):
    print('running test_merge...')
    for i, case in enumerate(cases):
      print('case', i + 1)
      self.assertDictEqual(merge(*case['test']), case['answer'])

  def test_useless_1(self):
    print('running test_useless_1...')
    self.assertFalse(10 < 4)

  def test_useless_2(self):
    print('running test_useless_2...')
    self.assertFalse(10 < 4)

if __name__ == "__main__":
  with open(XML_RESULT_FILE, mode='w', encoding='utf-8') as output:
    unittest.main(testRunner=xmlrunner.XMLTestRunner(output=output))
  
